---
title: "Introduction to Bayesian methods to evaluate population immunity coverage"
---


:::: {.columns}

::: {.column width='50%'}
[![](src/img/thumb_foundations.png){width="300px"}](src/1_foundations.qmd){target="_blank"}

Foundations

[![](src/img/thumb_implementation.png)](https://gitlab.cirad.fr/astre/training/serology-ppr-kano/-/raw/main/src/PPR-analysis-template.R?ref_type=heads&inline=false)

Implementation

:::

::: {.column width='50%'}
[![](src/img/thumb_inference.png){width="300px"}](src/2_inference.qmd){target="_blank"}

Inference

[![](src/img/thumb_implementation.png)](https://gitlab.cirad.fr/astre/training/serology-ppr-kano/-/raw/main/src/report-template.qmd?ref_type=heads&inline=false)

Report

:::

::::


- Download [data and code](https://gitlab.cirad.fr/astre/training/serology-ppr-kano/-/archive/main/serology-ppr-kano-main.zip)

- Report template [[PDF](src/report-template.pdf)] [[docx](src/report-template.docx)]
