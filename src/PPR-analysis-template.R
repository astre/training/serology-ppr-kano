## Template script for analysing the results of PPR serological surveys.
##
## Copyright 2024 Facundo Muñoz
## License: GNU GPL-3 https://www.gnu.org/licenses/gpl-3.0.en.html
##
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by the
## Free Software Foundation, either version 3 of the license, or
## (at your option) any later version.
## This program is distributed in the hope that it will be useful, but WITHOUT ANY
## WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
## General Public License for more details.
## You should have received a copy of the GNU General Public License with this
## program. If not, see <https://www.gnu.org/licenses/>.


# Packages ----------------------------------------------------------------

library(distributional)
library(extraDistr)
library(here)
library(janitor)
library(kableExtra)
library(readxl)
library(sf)
library(tidyverse)


# Functions ---------------------------------------------------------------

## Load helper functions stored in a separate file.
source(here("src/functions.R"))


# Read raw data -----------------------------------------------------------

## Serological data
ppr_animal_raw <- read_excel(here("data/20240522_PPR_sero_Kano.xlsx"))

## Data about sampled villages (e.g. nbr of animals)
sampled_villages_raw <- read_excel(here("data/20240210_Village_Kano.xlsx"))

## Full list of villages by LGA
kano_villages_raw <- read_excel(
  here("data/Final Kano Villages.xlsx"),
  range = cell_cols("A:D")
)


## Interpretation of serological values in terms of status
ppr_status_thresholds <- read.csv(here("data/ppr-status-thresholds.csv")) |>
  mutate(status = fct_inorder(status))


## Geographical data
kano_lga_geo_raw <- read_sf(here("data/Kano_LGA.geojson")) |>
  filter(NAME_1 == "Kano")


# Clean up data -----------------------------------------------------------

## Select relevant variables for the analysis
sampled_villages <- sampled_villages_raw |>
  select(
    village_id_raw = village_id,
    team_id,
    mission_id,
    village_order,
    village = village_name,
    district,
    lga,
    state,
    sr_nb
  ) |>
  mutate(
    village_id = encode_ids(
      state, lga, district, team_id, mission_id, village_order
    )
  ) |>
  ## Fix LGA names written differently from the cartography of reference
  mutate(
    lga = lga |>
      gsub(pattern = "Ungoggo", replacement = "Ungogo") |>
      gsub(pattern = "Danbatta", replacement = "Dambatta")
  ) |>
  ## We don't need these any more
  select(-team_id, -mission_id, -village_order)

## Check village ids
## This should return 0 lines if everything was ok
sampled_villages |>
  filter(village_id != village_id_raw)
## Note: compositional ids are generally not a good idea.
## They introduce redundancy and therefore opportunities for
## inconsistencies. Moreover, unicity is not guaranteed.
## Better use a simple numeric id or compute them on the fly
## as I have done here.

kano_lgas <- kano_villages_raw |>
  group_by(lga = LGA) |>
  summarise(
    n_villages = n(),
    animal_density = mean(`Animal density`),
    strata = mean(Strata)
  ) |>
  ## Fix LGA names written differently from the cartography of reference
  mutate(
    lga = lga |>
      gsub(pattern = "Danbatta", replacement = "Dambatta") |>
      gsub(pattern = "Garun Mallam", replacement = "Garum Mallam")
  )

## Fix a few abbreviated LGA names in the cartography
kano_lga_geo <- kano_lga_geo_raw |>
  mutate(
    lga = NAME_2 |>
      gsub(pattern = "DawakinK", replacement = "Dawakin Kudu") |>
      gsub(pattern = "DawakinT", replacement = "Dawakin Tofa") |>
      gsub(pattern = "RiminGad", replacement = "Rimin Gado")
  )


## Check all LGA names in the table of LGAs match those in the cartography
## This should return an empty set.
setdiff(kano_lgas$lga, kano_lga_geo$lga)


## Sample of animal serology (PI) by village, district and LGA
ppr_animal <- ppr_animal_raw |>
  ## Select the relevant variables (columns)
  select(
    animal          = 'animal_id',
    pi              = '%S/P',
    status_raw = 'sero_result'
  ) |>
  ## Filter observations (lines) if relevant
  filter(!is.na(pi)) |>
  ## Extract village id from animal id
  mutate(
    village_id = extract_id_term(animal, 1:6)
  ) |>
  ## Get village, district and LGA names from table of sampled villages
  left_join(
    sampled_villages |>
      select(village_id, village, district, lga),
    by = 'village_id'
  )



# 1: status of animals ---------------------------------------------------

ppr_animal <- ppr_animal |>
  ## Calculate the serological status at the animal level using
  ## the table of thresholds for the PPR kit employed.
  mutate(
    status = ppr_status(pi, ppr_status_thresholds)
  )

## Check. Compare the previously recorded status values
## with the calculated ones.
ppr_animal |>
  count(status, status_raw)

## Note: Not good to store status, which is a variable calculated from
## the %S/N values and the thresholds given by the provider of the kit.
## This opens opportunities for inconsistencies.

# 2.1: statistics at village level ----------------------------------------

ppr_villages <- ppr_animal |>
  select(-status_raw) |>
  group_by(
    ## Ignore the district level
    village_id, village, lga
  ) |>
  summarise(
    n_pos = sum(status == 'Positive'),
    n_dou = sum(status == 'Doubtful'),
    ## The number of negatives may or may not include doubtful observations
    ## depending on the total number of non-doubtful observations.
    ## (PRAPS protocol)
    n_neg = ppr_negatives(
      pos = n_pos,
      dou = n_dou,
      neg = sum(status == 'Negative')
    ),
    .groups = "drop"
  )


# 2.2: seroprevalence villages --------------------------------------------

ppr_villages <- ppr_villages |>
  rowwise() |>
  mutate(
    ## Density curves for the posterior densities
    post_density_seroprevalence = list(pdens_seroprevalence(
      x   = n_pos,
      n   = n_pos + n_neg,
      mu0 = 0.28,
      phi = 3
    )),
    ## Posterior distributions
    post_dist_seroprevalence = post_seroprevalence(
      x   = n_pos,
      n   = n_pos + n_neg,
      mu0 = 0.28,
      phi = 3
    ),
    ## Uncertainty intervals and point estimate
    post_sero_ll = unclass(hdr(post_dist_seroprevalence, size = 89))$lower[[1]],
    post_sero_hh = unclass(hdr(post_dist_seroprevalence, size = 89))$upper[[1]],
    post_sero_med = median(post_dist_seroprevalence)
  )

# 2.3 : prob. prot. villages ----------------------------------------------

ppr_villages <- ppr_villages |>
  ## Add total number of animals
  left_join(
    sampled_villages |>
      select(village_id, N_tot = sr_nb),
    by = 'village_id'
  ) |>
  mutate(
    ## Probability of protection
    p_protection = prob_protection(
      x   = n_pos,
      n   = n_pos + n_neg,
      N   = N_tot,    ## Total number of animals in the unit
      ## Prior information
      mu0 = 0.28,
      phi = 3,
      ## Protection threshold
      tau = 0.7
    )
  )



# 3.1 : statistics at LGA level -------------------------------------------

ppr_lgas <- ppr_villages |>
  group_by(
    lga
  ) |>
  summarise(
    n_pos = sum(p_protection),
    n_ech = n()
  )



# 3.2: seroprevalence LGAs ------------------------------------------------

ppr_lgas <- ppr_lgas |>
  rowwise() |>
  mutate(
    ## Density curves for the posterior densities
    post_density_seroprevalence = list(pdens_seroprevalence(
      x   = n_pos,
      n   = n_ech,
      mu0 = 0.2,
      phi = 7.6
    )),
    ## Posterior distributions
    post_dist_seroprevalence = post_seroprevalence(
      x   = n_pos,
      n   = n_ech,
      mu0 = 0.2,
      phi = 7.6
    ),
    ## Uncertainty intervals and point estimate
    post_sero_ll = unclass(hdr(post_dist_seroprevalence, size = 89))$lower[[1]],
    post_sero_hh = unclass(hdr(post_dist_seroprevalence, size = 89))$upper[[1]],
    post_sero_med = median(post_dist_seroprevalence)
  )


# 3.3: prob. prot. LGAs ---------------------------------------------------

ppr_lgas <- ppr_lgas|>
  ## Add the number of villages per LGA
  left_join(
    kano_lgas |>
      select(lga, n_villages),
    by = 'lga'
  ) |>
  mutate(
    ## Compute probability of protection
    p_protection = prob_protection(
      x   = n_pos,
      n   = n_ech,
      N   = n_villages,
      ## Prior information
      mu0 = 0.2,
      phi = 7.6,
      ## Protection threshold
      tau = 0.7
    )
  )


## Check I didn't miss any LGA in the join
## This should return 0 lines if everything is ok
ppr_lgas |>
  filter(is.na(n_villages))



# 4.1: posterior at Kano --------------------------------------------------

ppr_kano_post <-
  ppr_lgas |>
  ungroup() |>
  mutate(
    sample_props = generate(post_dist_seroprevalence, 1000) |>
      map(~enframe(., name = "rep", value = "prop")),
  ) |>
  select(lga, n_pos, n_ech, n_villages, sample_props) |>
  unnest(sample_props) |>
  mutate(
    draw_n_pos_total = prop * n_villages
  ) |>
  group_by(rep) |>
  summarise(
    n_pos = sum(draw_n_pos_total),
    n_tot = sum(n_villages),
    prop = n_pos / n_tot
  )

# 4.2 : global proportion of protected villages ---------------------------

## Median and 89% highest-density interval from drew proportions
ppr_kano <- ppr_kano_post |>
  ggdist::median_qi(prop, .width = 0.89)


# Export results ----------------------------------------------------------

# writexl::write_xlsx(
#   list(
#     villages = ppr_villages,
#     lgas = ppr_lgas,
#     kano = ppr_kano
#   ),
#   "reports/analysis-results.xlsx"
# )

