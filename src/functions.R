#' Categorisation of serological status
#'
#' This function encodes the thresholds given by the manufacturer to interpret
#' the ratios of optical density.
#'
#' @param x Numeric vector.
#' @param thresholds Data.frame with one row per category.
#'
#' @return Character vector.
#' @examples
#' ppr_bounds <- data.frame(
#'   lim_inf = c(NA, .5, .6),
#'   lim_sup = c(.5, .6, NA),
#'   status = c("P", "D", "N")
#' )
#' ppr_status(x = c(.3, .55, .8, 1.1, -0.1), thresholds = ppr_bounds)
ppr_status <- function(x, thresholds) {

  expand_grid(x = x, thresholds) |>
    filter(x <= lim_sup | is.na(lim_sup)) |>
    filter(x > lim_inf | is.na(lim_inf)) |>
    pull(status)

}


#' Determination of the number of negative individuals in a village
#'
#' Following the PRAPS protocol, the doubtful samples should be treated as
#' negative whenever ignoring them would leave too few (<9) observations.
#'
#' @examples
#' ppr_negatives(pos = 6, dou = 5, neg = 4)  # 4 negatives (ignore doubtful )
#' ppr_negatives(pos = 5, dou = 5, neg = 3)  # 8 negatives (include doubtful)
ppr_negatives <-
  function(pos, dou, neg) {
    case_when(
      pos + neg >= 9 ~ neg,
      pos + neg < 9 ~ neg + dou
    )
  }



#' Extract variables encoded in a string
#'
#' Given a character vector where each item is composed by several terms
#' separated by a symbol, return a vector of the i-th term of each item.
#'
#' @param x Character vector with structured items.
#' @param i Integer vector. Index or indices of the target term or terms.
#'
#' @return Character vector of terms.
#' @examples
#' extract_id_term(c("Fire Fox", "Thunder Bird"), 2, sep = " ")
#' extract_id_term(x = paste(LETTERS[1:4], collapse = "-"), i = 2:3)
extract_id_term <- function(x, i, sep = "-") {
  ## Split the id into separate terms
  xs <- strsplit(x, sep)

  ## Take the i-th term of each id and return a character vector
  xi <- map_chr(xs, function(id) paste(id[i], collapse = sep))

  return(xi)
}


#' Build encoded ids
#'
#' Reproduce the village and other ids by concatenating the first
#' 3 uppercase letters of a list of variables
#'
#' @param ... Variable names in a pipe, or character vectors.
#'
#' @return A character vector.
#'
#' @examples
#' encode_ids(c("Alpha", "Beta", "Gamma"), c("One", "Two", "Three"), c("V1", "V2", "V3"))
encode_ids <- function(...) {
  vars <- list(...)

  map(vars, ~toupper(substr(., 1, 3))) |>
    setNames(nm = paste0("v", seq_along(vars))) |>
    bind_cols() |>
    unite(
      col = "ans",
      sep = "-"
    ) |>
    pull(ans)
}



#' Posterior density seroprevalence
#'
#' Posterior density of the seroprevalence in a epidemiological unit from a
#' sample of serological results.
#'
#' @param x Number of seropositive samples.
#' @param n Sample size.
#' @param mu0 Prior mean of seroprevalence. Number between 0 and 1.
#' @param phi Prior precision of seroprevalence. A number greater or equal to 1,
#' which represents a number of samples that would provide an equivalent
#' degree of certitude.
#'
#' @examples
#' pdens_seroprevalence(x = 10, n = 12, mu0 = 0.28, phi = 3)
pdens_seroprevalence <- function(x, n, mu0, phi) {
  a0 <- mu0 * phi
  b0 <- (1 - mu0) * phi


  xx <- c(0, seq.int(20) / 20)
  yy <- dbeta(xx, shape1 = a0 + x, shape2 = b0 + n - x)

  ans <- data.frame(spline(xx, yy))

  return(ans)
}

#' Posterior distribution of the seroprevalence
#'
#' Posterior seroprevalence in a epidemiological unit from a
#' sample of serological results.
#'
#' @param x Number of seropositive samples.
#' @param n Sample size.
#' @param mu0 Prior mean of seroprevalence. Number between 0 and 1.
#' @param phi Prior precision of seroprevalence. A number greater or equal to 1,
#' which represents a number of samples that would provide an equivalent
#' degree of certitude.
#'
#' @import distributional
#' @examples
#' post_seroprevalence(x = 10, n = 12, mu0 = 0.28, phi = 3)
post_seroprevalence <- function(x, n, mu0, phi) {
  a0 <- mu0 * phi
  b0 <- (1 - mu0) * phi

  post <- dist_beta(shape1 = a0 + x, shape2 = b0 + n - x)

  return(post)
}


#' Probability of serological protection
#'
#' Compute a Bayesian probability of protection (i.e., seroprevalence greater
#' above a certain threshold) of a epidemiological unit from a sample of
#' serological results.
#'
#' @param x Number of seropositive samples.
#' @param n Sample size.
#' @param N Population size in the unit. If unknown, a rough estimation of its
#' magnitude will do.
#' @param mu0 Prior mean of seroprevalence. Number between 0 and 1.
#' @param phi Prior precision of seroprevalence. A number greater or equal to 1,
#' which represents a number of samples that would provide an equivalent
#' degree of certitude.
#' @param tau Threshold for protection. A number between 0 and 1.
#'
#' @importFrom extraDistr pbbinom
#' @examples
#' prob_protection(10, 12, 52, mu0 = 0.28, phi = 3, 0.7)
prob_protection <- function(x, n, N, mu0, phi, tau) {
  a0 <- mu0 * phi
  b0 <- (1 - mu0) * phi
  top_M <- floor(tau * N - 1e-10)
  ## Prevent probabilities slightly larger than 1 due to numerical precision
  cans <- pmin(
    1,
    pbbinom(top_M - x, size = N - n, alpha = a0 + x, beta = b0 + n - x)
  )
  1 - cans
}
