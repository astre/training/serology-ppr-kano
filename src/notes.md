# Lidiski PVE training

1. Understand the target quantities and explain why we compute these instead of a simple sample proportion

Demonstrate that we simply can't estimate the global prevalence without knowledge of the distribution of animals across the territory. 

Introduce the hierarchy of admin units and the idea of proportion of protected units at each level. 

Practice manual computations of sample proportions ignoring inference for now.

2. Inference

Representing knowledge and information with probability distributions.

Inference on a proportion using the Beta distribution.

3. Protocol of analysis

Combine these things together to produce estimates at each hierarchical level.