## Template script for analysing the results of PPR serological surveys.
##
## Copyright 2024 Facundo Muñoz
## License: GNU GPL-3 https://www.gnu.org/licenses/gpl-3.0.en.html
##
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by the
## Free Software Foundation, either version 3 of the license, or
## (at your option) any later version.
## This program is distributed in the hope that it will be useful, but WITHOUT ANY
## WARRANTY; without even the implied warranty of MERCHANTABILITY or
## FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
## General Public License for more details.
## You should have received a copy of the GNU General Public License with this
## program. If not, see <https://www.gnu.org/licenses/>.


# Packages ----------------------------------------------------------------

library(distributional)
library(extraDistr)
library(here)
library(janitor)
library(kableExtra)
library(readxl)
library(sf)
library(tidyverse)


# Functions ---------------------------------------------------------------

## Load helper functions stored in a separate file.
source(here("src/functions.R"))


# Read raw data -----------------------------------------------------------

## Serological data
ppr_animal_raw <- read.csv(here("data/Bauchi_avh.csv"), sep = ";", dec = ".")

## Full list of villages by LGA
bauchi_villages_raw <- read_excel(
  here("data/Bauchi_list_villages.xlsx")
)


## Interpretation of serological values in terms of status
ppr_status_thresholds <- read.csv(here("data/ppr-status-thresholds.csv")) |>
  mutate(status = fct_inorder(status))


## Geographical data
bauchi_lga_geo_raw <- read_sf(here("data/Plateau_LGA.shp")) |>
  filter(NAME_1 == "Bauchi")


# Clean up data -----------------------------------------------------------

## Select relevant variables for the analysis
sampled_villages <- ppr_animal_raw |>
  distinct(
    village_id,
    # team_id = Team.ID,
    # mission_id = Mission.ID,
    # village_order = Vorder,
    long,
    lat,
    village = village_name,
    lga,
    district,
    sr_nb
  ) |>
  ## Use a total number of 200 animals in villages with missing values
  mutate(
    # sr_nb = coalesce(sr_nb, 200)
  )
  ## Fix LGA names written differently from the cartography of reference
  # mutate(
  #   lga = lga |>
  #     gsub(pattern = "Ungoggo", replacement = "Ungogo") |>
  #     gsub(pattern = "Danbatta", replacement = "Dambatta")
  # ) |>
  # ## We don't need these any more
  # select(-team_id, -mission_id, -village_order)


bauchi_lgas <- bauchi_villages_raw |>
  ## Fill up missing values for LGA, and district.
  fill(
    LGA, District
  ) |>
  group_by(lga = LGA) |>
  summarise(
    n_villages = n(),
    # animal_density = mean(`Animal density`),
    # strata = mean(Strata)
  ) #|>
  ## Fix LGA names written differently from the cartography of reference
  # mutate(
  #   lga = lga |>
  #     gsub(pattern = "Danbatta", replacement = "Dambatta") |>
  #     gsub(pattern = "Garun Mallam", replacement = "Garum Mallam")
  # )

# ## Fix a few abbreviated LGA names in the cartography
# bauchi_lga_geo <- bauchi_lga_geo_raw |>
#   mutate(
#     lga = NAME_2 |>
#       gsub(pattern = "Qua'anpa", replacement = "Qua'an Pan")
#   )


## Check all LGA names in the table of LGAs match those in the cartography
## This should return an empty set.
# setdiff(bauchi_lgas$lga, bauchi_lga_geo$lga)


## Sample of animal serology (PI) by village, district and LGA
ppr_animal <- ppr_animal_raw |>
  ## Select the relevant variables (columns)
  select(
    animal          = 'animal_id',
    pi              = 'ELISA_per',
    village_id,,
    village         = village_name,
    lga
  ) |>
  mutate(
    pi = pi / 100
  #   lga = lga |>
  #     tools::toTitleCase() |>
  #     gsub(pattern = "Quanpan", replacement = "Qua'an Pan")
  ) |>
  ## Filter observations (lines) if relevant
  filter(!is.na(pi))




# 1: status of animals ---------------------------------------------------

ppr_animal <- ppr_animal |>
  ## Calculate the serological status at the animal level using
  ## the table of thresholds for the PPR kit employed.
  mutate(
    status = ppr_status(pi, ppr_status_thresholds)
  )


# 2.1: statistics at village level ----------------------------------------

ppr_villages <- ppr_animal |>
  group_by(
    ## Ignore the district level
    village_id, village, lga
  ) |>
  summarise(
    n_pos = sum(status == 'Positive'),
    n_dou = sum(status == 'Doubtful'),
    ## The number of negatives may or may not include doubtful observations
    ## depending on the total number of non-doubtful observations.
    ## (PRAPS protocol)
    n_neg = ppr_negatives(
      pos = n_pos,
      dou = n_dou,
      neg = sum(status == 'Negative')
    ),
    .groups = "drop"
  )


# 2.2: seroprevalence villages --------------------------------------------

ppr_villages <- ppr_villages |>
  rowwise() |>
  mutate(
    ## Density curves for the posterior densities
    post_density_seroprevalence = list(pdens_seroprevalence(
      x   = n_pos,
      n   = n_pos + n_neg,
      mu0 = 0.28,
      phi = 3
    )),
    ## Posterior distributions
    post_dist_seroprevalence = post_seroprevalence(
      x   = n_pos,
      n   = n_pos + n_neg,
      mu0 = 0.28,
      phi = 3
    ),
    ## Uncertainty intervals and point estimate
    post_sero_ll = unclass(hdr(post_dist_seroprevalence, size = 89))$lower[[1]],
    post_sero_hh = unclass(hdr(post_dist_seroprevalence, size = 89))$upper[[1]],
    post_sero_med = median(post_dist_seroprevalence)
  )

# 2.3 : prob. prot. villages ----------------------------------------------

ppr_villages <- ppr_villages |>
  ## Add total number of animals
  left_join(
    sampled_villages |>
      select(village_id, N_tot = sr_nb),
    by = 'village_id'
  ) |>
  mutate(
    ## Probability of protection
    p_protection = prob_protection(
      x   = n_pos,
      n   = n_pos + n_neg,
      N   = N_tot,    ## Total number of animals in the unit
      ## Prior information
      mu0 = 0.28,
      phi = 3,
      ## Protection threshold
      tau = 0.7
    )
  )



# 3.1 : statistics at LGA level -------------------------------------------

ppr_lgas <- ppr_villages |>
  group_by(
    lga
  ) |>
  summarise(
    n_pos = sum(p_protection),
    n_ech = n()
  )



# 3.2: seroprevalence LGAs ------------------------------------------------

ppr_lgas <- ppr_lgas |>
  rowwise() |>
  mutate(
    ## Density curves for the posterior densities
    post_density_seroprevalence = list(pdens_seroprevalence(
      x   = n_pos,
      n   = n_ech,
      mu0 = 0.2,
      phi = 7.6
    )),
    ## Posterior distributions
    post_dist_seroprevalence = post_seroprevalence(
      x   = n_pos,
      n   = n_ech,
      mu0 = 0.2,
      phi = 7.6
    ),
    ## Uncertainty intervals and point estimate
    post_sero_ll = unclass(hdr(post_dist_seroprevalence, size = 89))$lower[[1]],
    post_sero_hh = unclass(hdr(post_dist_seroprevalence, size = 89))$upper[[1]],
    post_sero_med = median(post_dist_seroprevalence)
  )


# 3.3: prob. prot. LGAs ---------------------------------------------------

ppr_lgas <- ppr_lgas |>
  ## Add the number of villages per LGA
  left_join(
    bauchi_lgas |>
      select(lga, n_villages),
    by = 'lga'
  ) |>
  mutate(
    ## Compute probability of protection
    p_protection = prob_protection(
      x   = n_pos,
      n   = n_ech,
      N   = n_villages,
      ## Prior information
      mu0 = 0.2,
      phi = 7.6,
      ## Protection threshold
      tau = 0.7
    )
  )


## Check I didn't miss any LGA in the join
## This should return 0 lines if everything is ok
ppr_lgas |>
  filter(is.na(n_villages))



# 4.1: posterior at Bauchi --------------------------------------------------

ppr_bauchi_post <-
  ppr_lgas |>
  ungroup() |>
  mutate(
    sample_props = generate(post_dist_seroprevalence, 1000) |>
      map(~enframe(., name = "rep", value = "prop")),
  ) |>
  select(lga, n_pos, n_ech, n_villages, sample_props) |>
  unnest(sample_props) |>
  mutate(
    draw_n_pos_total = prop * n_villages
  ) |>
  group_by(rep) |>
  summarise(
    n_pos = sum(draw_n_pos_total, na.rm = TRUE),
    n_tot = sum(n_villages, na.rm = TRUE),
    prop = n_pos / n_tot
  )

# 4.2 : global proportion of protected villages ---------------------------

## Median and 89% highest-density interval from drew proportions
ppr_bauchi <- ppr_bauchi_post |>
  ggdist::median_qi(prop, .width = 0.89)


# Export results ----------------------------------------------------------

writexl::write_xlsx(
  list(
    villages = ppr_villages,
    lgas = ppr_lgas,
    bauchi = ppr_bauchi
  ),
  "reports/bauchi-results.xlsx"
)

