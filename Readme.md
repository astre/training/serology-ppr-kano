# Bayesian methods for the assessment of PPR immunity coverage

## Case study: Post-vaccinal evaluation in Kano State, Nigeria.

2024-11-04/07. Nigeria. Distance training. 1 day.

This is part of a 4-day training on _Biostatistics applied to epidemiology survey data_ in the context of the project [Lidiski](https://www.lidiski.org/).

The program consists of 4 parts:

1. Foundations: what are the target quantities.

2. Inference: how to model estimation uncertainty.

3. Implementation: review of a script that applies the methods in the specific case study.

4. Reporting: from the survey data to the technical report.


## Authors

Facundo Muñoz [![ORCID](https://orcid.org/assets/vectors/orcid.logo.icon.svg){width="20px"}](https://orcid.org/0000-0002-5061-4241)- statistical methodology, training

Marion Bordier [![ORCID](https://orcid.org/assets/vectors/orcid.logo.icon.svg){width="20px"}](https://orcid.org/0000-0002-0182-5210)- epidemiological application, case study

## Usage

Browse the [training website](https://astre.gitlab.cirad.fr/training/serology-ppr-kano/) for the slides, exercises, links to data, code and template report.

The current directory is a [Quarto website](https://quarto.org/docs/websites/). The directory `data` contains the data files used in the examples and exercises. `src` contains the source code for the analysis script, the helper functions and the report template, but also for all the slides, the interactive elicitation tool.


## Cite

Facundo Muñoz, Marion Bordier (2024). Bayesian methods for the assessment of PPR immunity coverage. [hal-04767809](https://hal.science/hal-04767809).


## License

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://astre.gitlab.cirad.fr/training/serology-ppr-kano">Bayesian methods for the assessment of PPR immunity coverage</a> by <span property="cc:attributionName">Facundo Muñoz and Marion Bordier at Cirad, UMR ASTRE</span> is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Creative Commons Attribution-ShareAlike 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1" alt=""></a></p>

---

[![](src/img/logo_EU-lidiski.jpg){height="50px"}](https://www.lidiski.org/) [![](src/img/Cirad-ASTRE_Eng.png){height="50px"}](https://umr-astre.cirad.fr/)
